This folder contains the files for RG with activity.

	rg_click.py : main file that contains all the functions.

	personalize_click.py : file that contains functions for testing during development.

	documentation.pdf : detailed documentation of the design and algorithm.
