'''
RG computation for:
1. Regularize and the normalize the RG score. 

2. RG score update with user click behaviors.

Written by Thomas Zhang: thomas@owler-inc.com on Oct 18th, 2017.
'''

import numpy as np


def rg_regularize(rg):
    ''' function for regularize and then normalize (static) RG.
	This process need to be called for current RG score to normalize and regularize it.
	After each time the RG service of follow and unfollow is called, this process may need to be called
	since that process would probably destroy all the information we have about clicks and normalization.
	'''
    rg = rg / float(max(rg))
    regrg = 100000 * np.sqrt(rg)
    regrg = np.asarray([int(e) for e in regrg])  # rewrite the results using INT?
    return regrg


def personalize(rg, click, count, G=50, kernel='linear'):
    ''' function for computing the rg score with activity based on
	user clicking history. This function need to be called once at the
	beginning and also need to be called whenever we have a follow and unfollow, since that
	process would probably destroy all the information with clicks data.
	Inputs
	--------
	rg_act : array, shape(n_company,)
		Current rg score with old activity information.

	click : ndarray, shape(t_day,n_company)
		Click matrix where click[i,j] equals the number of clicks on company-j on day-i.

	count : array, shape(t_day,)
		Total number of clicks on a given day.

	G : int
		Smoothing factor for RG sensitivity to click changes. 

	kernel : string, default 'linear'
		Time decay function. If set to 'none' clicks are treated equally.

	Outputs
	--------
	rg_act : unnormalized rg score with activity	
	'''

    # initialization
    N, T = click.shape[1], click.shape[0]
    rg_act = np.zeros(N)
    rg = rg / float(sum(rg))  # turn rg into a probability measure.

    # time decay functions
    if kernel == 'none':
        weightlist = np.ones(T)
    elif kernel == 'linear':
        minweight, maxweight = 0.5, 1.5
        weightlist = np.arange(minweight, maxweight, (maxweight - minweight) / T)

    # main computation
    for i in range(N):
        marg = 0.
        tot_c = np.dot(count, weightlist)
        for t in range(T):
            marg += click[t, i] * weightlist[t]
        rg_act[i] = (marg + G * rg[i]) / (tot_c + G)

    return rg_act


def normalize(rg_act):
    ''' normalize after all the computations'''
    rg_act = 100000 * rg_act / float(max(rg_act))
    rg_act = np.asarray([int(e) for e in rg_act])  # rewrite the results using INT?
    return rg_act


def post_processing(rg_act):
    ''' post-processing for rg score. Setting a minimum value for RG'''
    for i in range(len(rg_act)):
        rg_act[i] = max(rg_act[i], 100)
    return rg_act
